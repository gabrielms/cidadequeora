import React from 'react'
import { ConnectedRouter } from 'react-router-redux'
import { Route } from 'react-router'
import { default as NewVolunteer } from './containers/volunteer/newVolunteer.js'

const Router = ({ history }) =>
  <ConnectedRouter history={history}>
    <div>
      <Route exact path="/" component={NewVolunteer}/>
    </div>
  </ConnectedRouter>

export default Router