import React from 'react'
import createHistory from 'history/createHashHistory'
import { Provider } from 'react-redux'
import configureStore from '../services/configureStore.js'
import Router from '../router.js'

const history = createHistory()
const store = configureStore(history)

const Root = () =>
  <Provider store={store}>
    <Router history={history} />
  </Provider>

export default Root