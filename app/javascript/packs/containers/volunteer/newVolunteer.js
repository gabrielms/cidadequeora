import { connect } from 'react-redux'
import { default as View } from '../../components/volunteer/newVolunteer.js'
import { reduxForm, getFormValues, change } from 'redux-form'
import save from '../../actions/volunteer/save.js'
import { setCurrentStep } from '../../actions/volunteer/currentStep.js'
import fetchAddress from '../../actions/volunteer/fetchAddress.js'

function mapStateToProps(state) {
  console.log(state)
  return {
    volunteerAddress: getFormValues('volunteerAddress')(state),
    currentStep: state.currentStep
  }
}

const mapDispatchToProps = {
  onLocalizationSearchClick : (zipCode) => {
    return(dispatch) => {
      dispatch(
        fetchAddress(
          zipCode,
          (data) => {
            dispatch(change('volunteerAddress', 'street', data.street))
            dispatch(change('volunteerAddress', 'neighborhood', data.neighborhood))
            dispatch(change('volunteerAddress', 'city', data.city))
            dispatch(change('volunteerAddress', 'state', data.state))
          }
        )
      )
    }
  },
  onSetStep : (step) => {
    return(dispatch) => {
      dispatch(setCurrentStep(step))
    }
  },
  clearAddressFieldsArray : () => {
    return(dispatch) => {
      dispatch(change('volunteerAddress', 'excluded_addresses', []))
    }
  }
}

const Form = reduxForm({
  form: 'volunteerAddress',
  onSubmit: save
})(View)

const AddressRegister = connect(mapStateToProps, mapDispatchToProps)(Form)

export default AddressRegister