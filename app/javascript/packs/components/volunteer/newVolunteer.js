import React, {Component} from 'react'
import StepsHeader from './steps/stepsHeader'
import { Field } from 'redux-form'
import Input from '../../input.js'
import Checkbox from '../../checkbox.js'
import AddressRegisterStep from './steps/addressRegisterStep.js'
import NearbyResidencesStep from './steps/nearbyResidences/nearbyResidencesStep.js'
import ChurchStep from './steps/churchStep.js'
import CompletedStep from './steps/completedStep.js'

export default class NewVolunteer extends Component {
  render(){
    return (
      <div className="container">
        <StepsHeader step={this.props.currentStep.step}/>
        <form onSubmit={this.props.handleSubmit}>
          <AddressRegisterStep {...this.props} />
          <NearbyResidencesStep {...this.props} />
          <ChurchStep {...this.props} />
          <CompletedStep {...this.props} />
        </form>
      </div>
    )
  }
}