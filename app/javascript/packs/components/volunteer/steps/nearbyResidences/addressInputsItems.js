import React, { Component, PropTypes } from 'react'
import { AddressInputs } from './addressInputs'
import { Field, FieldArray } from 'redux-form'
import Input from '../../../../input.js'
import Checkbox from '../../../../checkbox.js'

export default class RenderAddresses extends Component {
  renderBuildingFields(excluded_address, index){

    if(!this.props.volunteerAddress.excluded_addresses[index].numbers_correspond_to_building){
      return null
    }
    return(
      <div>
        <Field
          component={Input}
          id={`${index}_building_name`}
          name={`${excluded_address}.building_name`}
          label="Nome do prédio ou condomínio"
        />

        <Field
          component={Checkbox}
          id={`${index}_deliver_for_all`}
          name={`${excluded_address}.deliver_for_all`}
          label="Entregar para todo o prédio/condomínio"
        />

        <Field
          component={Input}
          id={`${index}_apartment_numbers`}
          name={`${excluded_address}.apartment_numbers`}
          label="Número dos apartamentos e blocos que serão excetuados"
        />
      </div>
    )
  }
  render(){
    return (
      <div>
        {this.props.fields.map((excluded_address, index) =>
          <div key={index}>
            <hr />
            <Field
              component={Input}
              id={`${index}_zip_code`}
              name={`${excluded_address}.zip_code`}
              label="CEP"
            />

            <Field
              component={Input}
              id={`${index}_street`}
              name={`${excluded_address}.street`}
              label="Endereço"
            />

            <b>Digite os números das residências desta rua que você fará as distribuições</b>
            <Field
              component={Input}
              id={`${index}_numbers`}
              name={`${excluded_address}.numbers`}
            />

            <Field
              component={Checkbox}
              id={`${index}_numbers_correspond_to_building`}
              name={`${excluded_address}.numbers_correspond_to_building`}
              label="A numeração informada corresponde a um prédio ou condomínio"
            />

            {this.renderBuildingFields(excluded_address, index)}
          </div>
        )}
      <button type="button" onClick={() => this.props.fields.push({})}>Adicionar mais um endereço</button>
    </div>
    )
  }
}