import React, {Component} from 'react'
import { change, Field, FieldArray } from 'redux-form'
import Input from '../../../../input.js'
import Checkbox from '../../../../checkbox.js'
import AddressInputs from './addressInputs.js'
import AddressInputsItems from './addressInputsItems.js'

export default class NearbyResidencesStep extends Component {
  onLocalizationSearchClick = () => {
    this.props.onLocalizationSearchClick(this.props.volunteerAddress.address_search)
  }
  onExceptNearbyResidencesChange = () => {
    if(!this.props.volunteerAddress.except_nearby_residences){
      this.props.clearAddressFieldsArray()
    }
  }
  render(){
    if(this.props.currentStep.step != 2){
      return null
    }
    return (
      <div>
        <div className="row justify-content-center">
          <div className="col-5 steps-title">
            Residências que quero <b> distribuir </b>
          </div>
        </div>
        <div className="row">
          <div className="inputs-container col-12">
            <div className="form-group">
              <Field
                component={Checkbox}
                label="Além da minha residência, quero excetuar outras residências próximas à minha."
                name="except_nearby_residences"
                onChange={this.onExceptNearbyResidencesChange}
              />
            </div>

            <AddressInputs {...this.props} />

            <FieldArray
              name="excluded_addresses"
              component={AddressInputsItems}
              {...this.props}
            />
          </div>
        </div>
        <div className="row">
          <button className="btn btn-next" onClick={this.props.onSetStep.bind(this, 3)}>
            Próximo
          </button>
        </div>
      </div>
    )
  }
}