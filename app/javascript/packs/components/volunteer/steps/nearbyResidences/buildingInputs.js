import React, {Component} from 'react'
import { Field } from 'redux-form'
import Input from '../../../../input.js'
import Checkbox from '../../../../checkbox.js'

export default class BuildingInputs extends Component {
  render(){
    if(!this.props.volunteerAddress.numbers_correspond_to_building){
      return null
    }
    return (
      <div>
        <div className="row">
          <div className="col-8">
            <Field
              component={Input}
              name="building_name"
              label="Nome do prédio ou condomínio"
            />
          </div>
          <div className="col-4">
            <Field
              component={Checkbox}
              name="deliver_for_all"
              label="Entregar para todo o prédio/condomínio"
            />
          </div>
        </div>

        <Field
          component={Input}
          name="apartment_numbers"
          label="Número dos apartamentos e blocos que serão excetuados"
        />
      </div>
    )
  }
}