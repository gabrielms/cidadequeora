import React, {Component} from 'react'
import { Field } from 'redux-form'
import Input from '../../../../input.js'
import Checkbox from '../../../../checkbox.js'
import BuildingInputs from './buildingInputs.js'

export default class AddressInputs extends Component {
  render(){
    if(!this.props.volunteerAddress.except_nearby_residences){
      return null
    }

    return (
      <div>
        <hr />
        <Field
          component={Input}
          id="zip_code"
          name="zip_code"
          label="CEP"
          disabled
        />

        <Field
          component={Input}
          id="street"
          name="street"
          label="Endereço"
          disabled
        />

        <b>Digite os números das residências desta rua que você fará as distribuições</b>
        <Field
          component={Input}
          id="numbers"
          name="numbers"
          label=""
        />

        <Field
          component={Checkbox}
          id="numbers_correspond_to_building"
          name="numbers_correspond_to_building"
          label="A numeração informada corresponde a um prédio ou condomínio"
        />

        <BuildingInputs {...this.props} />

      </div>
    )
  }
}