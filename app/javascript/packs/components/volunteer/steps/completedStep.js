import React, {Component} from 'react'
import { Field } from 'redux-form'

export default class CompletedStep extends Component {
  render(){
    if(this.props.currentStep.step != 4){
      return null
    }

    return (
      <div>
        <div className="row justify-content-center completed-container">
          <div className="col col-4">
            <i className="fa fa-5x fa-check" aria-hidden="true"></i>
          </div>
          <b>Seu cadastro foi realizado com sucesso!</b>
          <button onClick={this.props.onSetStep.bind(this, 1)} className="btn btn-next">
            Voltar ao início
          </button>
        </div>
      </div>
    )
  }
}