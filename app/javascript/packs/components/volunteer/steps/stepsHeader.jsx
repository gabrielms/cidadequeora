import React, {Component} from 'react'

export default class StepsHeader extends Component {
  render(){
    if(this.props.step == 4){
      return null
    }
    const classNameFirstStep = "active-step"
    const classNameSecondStep = this.props.step >= 2 ? "active-step" : "inactive-step"
    const classNameThirdStep = this.props.step >= 3 ? "active-step" : "inactive-step"

    return (
      <div>
        <div className="row steps-header">
          <div className="col-12 text-center">
            <span className={"fa-stack fa-2x " + classNameFirstStep}>
              <i className={"fa fa-circle fa-stack-2x "  + classNameFirstStep}></i>
              <i className="fa fa-map-marker fa-stack-1x fa-inverse"></i>
            </span>
            <span className={"step-line " + classNameFirstStep} />
            <span className={"fa-stack fa-2x " + classNameSecondStep}>
              <i className={"fa fa-circle fa-stack-2x " + classNameSecondStep}></i>
              <i className="fa fa-object-group fa-stack-1x fa-inverse"></i>
            </span>
            <span className="step-line" />
            <span className={"fa-stack fa-2x " + classNameThirdStep}>
              <i className={"fa fa-circle fa-stack-2x " + classNameThirdStep}></i>
              <i className="fa fa-home fa-stack-1x fa-inverse"></i>
            </span>
          </div>
        </div>
      </div>
    )
  }
}