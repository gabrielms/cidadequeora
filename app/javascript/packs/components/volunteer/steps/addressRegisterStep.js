import React, {Component} from 'react'
import { Field } from 'redux-form'
import Input from '../../../input.js'
import Checkbox from '../../../checkbox.js'

export default class AddressRegisterStep extends Component {
  onLocalizationSearchClick = () => {
    this.props.onLocalizationSearchClick(this.props.volunteerAddress.zip_code)
  }
  render(){
    if(this.props.currentStep.step != 1){
      return null
    }
    return (
      <div>
        <div className="row justify-content-center">
          <div className="col-5 steps-title">
            Realize seu <b> cadastro </b> como <b> voluntário </b>
          </div>
        </div>
        <div className="inputs-container">
          <div className="row">
            <div className="col-12">
              <Field
                component={Input}
                name="name"
                label="Nome"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              <Field
                component={Input}
                name="email"
                label="E-mail"
              />
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <label htmlFor="cell_phone_area_code">Telefone celular</label>
            </div>
          </div>
          <div className="row">
            <div className="col-2">
              <Field
                component={Input}
                name="cell_phone_area_code"
              />
            </div>
            <div className="col-6">
              <Field
                component={Input}
                name="cell_phone"
              />
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <label htmlFor="residential_phone_area_code">Telefone residencial</label>
            </div>
          </div>
          <div className="row">
            <div className="col-2">
              <Field
                component={Input}
                name="residential_phone_area_code"
              />
            </div>
            <div className="col-6">
              <Field
                component={Input}
                name="residential_phone"
              />
            </div>
            <div className="col-2">
              <Field
                component={Checkbox}
                name="cell_phone_whatsapp"
              />
              Whatsapp
              <i className="fa fa-whatsapp" />
            </div>
          </div>
        </div>

          <div className="inputs-container-blank">
            <div className="row justify-content-center">
              <b className="search-label"> Digite seu CEP ou nome da sua rua </b>
            </div>
            <div className="row justify-content-center">
              <div className="form-inline">
                <Field
                  component={Input}
                  name="zip_code"
                />
                <button
                  onClick={this.onLocalizationSearchClick}
                  type="button"
                  className="btn btn-search">
                  Pesquisar
                </button>
              </div>
            </div>

            <div className="row">
              <div className="col-12">
                <Field
                  component={Input}
                  name="street"
                  label="Logradouro"
                />
              </div>
            </div>
            <div className="row">
              <div className="col col-3">
                <Field
                  component={Input}
                  name="number"
                  label="Número"
                />
              </div>
              <div className="col col-9">
                <Field
                  component={Input}
                  name="complement"
                  label="Complemento"
                />
              </div>
            </div>

            <div className="row">
              <div className="col col-4">
                <Field
                  component={Input}
                  name="neighborhood"
                  label="Bairro"
                />
              </div>

              <div className="col col-5">
                <Field
                  component={Input}
                  name="city"
                  label="Cidade"
                />
              </div>

              <div className="col col-3">
                <Field
                  component={Input}
                  name="state"
                  label="UF"
                />
              </div>

              <div className="row">
                <button className="btn btn-next pull-right" onClick={this.props.onSetStep.bind(this, 2)}>
                  Próximo
                </button>
              </div>
            </div>
          </div>
      </div>
    )
  }
}