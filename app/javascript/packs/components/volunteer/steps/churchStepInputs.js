import React, {Component} from 'react'
import { Field } from 'redux-form'
import Input from '../../../input.js'

export default class ChurchStepInputs extends Component {
  render(){
    if(this.props.volunteerAddress.part_of_church === "no"){
      return null
    }

    return (
      <div>
        <hr />
        <Field
          component={Input}
          name="church_name"
          id="church_name"
          label="Nome da igreja"
        />

        <div className="row">
          <div className="col col-4">
            <Field
              component={Input}
              name="church_zip_code"
              id="church_zip_code"
              label="CEP"
            />
          </div>

          <div className="col col-8">
            <Field
              component={Input}
              name="church_address"
              id="church_address"
              label="Logradouro"
            />
          </div>
        </div>

        <div className="row">
          <div className="col col-3">
            <Field
              component={Input}
              name="church_number"
              id="church_number"
              label="Número"
            />
          </div>

          <div className="col col-9">
            <Field
              component={Input}
              name="church_complement"
              id="church_complement"
              label="Complemento"
            />
          </div>
        </div>

        <div className="row">
          <div className="col col-4">
            <Field
              component={Input}
              name="church_neighborhood"
              id="church_neighborhood"
              label="Bairro"
            />
          </div>

          <div className="col col-6">
            <Field
              component={Input}
              name="church_city"
              id="church_city"
              label="Cidade"
            />
          </div>

          <div className="col col-2">
            <Field
              component={Input}
              name="church_state"
              id="church_state"
              label="UF"
            />
          </div>
        </div>
        <Field
          component={Input}
          name="church_leader"
          id="church_leader"
          label="Nome do líder principal"
        />
      </div>
    )
  }
}