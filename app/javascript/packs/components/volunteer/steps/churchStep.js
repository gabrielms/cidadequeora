import React, {Component} from 'react'
import { Field } from 'redux-form'
import ChurchStepInputs from './churchStepInputs.js'
import Input from '../../../input.js'

export default class ChurchStep extends Component {
  render(){
    if(this.props.currentStep.step != 3){
      return null
    }

    return (
      <div>
        <div className="row justify-content-center">
          <div className="col-5 steps-title">
            <b> Igreja </b> que frequenta
          </div>
        </div>

        <div className="inputs-container">
          <div className="row justify-content-center">
            Faz parte de alguma igreja?
          </div>
          <div className="row justify-content-center">
            <label>
              <Field name="part_of_church" component="input" type="radio" value="yes"/>
              Sim
            </label>
            <label>
              <Field name="part_of_church" component="input" type="radio" value="no"/>
              Não
            </label>
          </div>
          <ChurchStepInputs {...this.props} />
        </div>

        <button className="btn btn-next" type="submit">
          Salvar
        </button>
      </div>
    )
  }
}