import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as formReducer } from 'redux-form'
import { default as currentStepReducer } from './currentStep.js'

const reducers = combineReducers({
  router: routerReducer,
  form: formReducer,
  currentStep: currentStepReducer
})

export default reducers