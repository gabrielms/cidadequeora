import { ADDRESS_REGISTER_STEP } from '../constants/steps.js'
import { SET_CURRENT_STEP } from '../actions/volunteer/currentStep.js'

const initialState = {
  step: ADDRESS_REGISTER_STEP
}

export default function currentStep(state = initialState, action){
  switch(action.type){
    case SET_CURRENT_STEP:
      return { step: action.step }
    default:
      return state
  }
}