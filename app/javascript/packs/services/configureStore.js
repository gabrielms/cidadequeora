import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import { applyMiddleware, createStore } from 'redux'
import reducers from '../reducers/index.js'

export default function configureStore(history) {
  const middleware = applyMiddleware(routerMiddleware(history), thunk)

  return createStore(reducers, middleware)
}