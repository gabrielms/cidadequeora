import React, { PropTypes } from 'react'

const Input = props => {
  const labelInput = props.label ? <label htmlFor={props.input.name}>{props.label}</label> : ""
  return (
    <div className="form-group">
      {labelInput}
      <input
        type="text"
        id={props.input.name}
        name={props.input.name}
        value={props.input.value}
        className="form-control"
        onChange={props.input.onChange}
        disabled={props.disabled}
      />
    </div>
  )
}

export default Input