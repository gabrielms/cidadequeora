import axios from 'axios'
import { reset, SubmissionError } from 'redux-form'
import { setCurrentStep } from './currentStep.js'

export default function save(values, dispatch, props) {
  const url = window.location.origin + '/volunteers.json'

  const excludedActualAddress = [{
    zip_code: values.zip_code,
    street: values.street,
    numbers: values.numbers,
    numbers_correspond_to_building: values.numbers_correspond_to_building,
    apartment_numbers: values.apartment_numbers,
    deliver_for_all: values.deliver_for_all
  }]
  const addresses = values.excluded_addresses || []
  const excludedAddresses = addresses.concat(excludedActualAddress)

  const data = {
    volunteer: {
      name: values.name,
      email: values.email,
      address_attributes: {
        zip_code: values.zip_code,
        city: values.city,
        complement: values.complement,
        house_number: values.house_number,
        state: values.state,
        street: values.street
      },
      church_attributes: {
        name: values.church_name,
        leader: values.church_leader,
        address_attributes: {
          zip_code: values.church_zip_code,
          city: values.church_city,
          complement: values.church_complement,
          house_number: values.church_house_number,
          state: values.church_state,
          street: values.church_street
        }
      },
      excluded_addresses_attributes: excludedAddresses
    }
  }

  return axios({
    url,
    method: 'POST',
    data,
  })
  .then((response) => {
    dispatch(setCurrentStep(4))
    dispatch(reset('volunteerAddress'))
  })
  .catch((error) => {
    throw new SubmissionError({
      status: error.response.status,
      statusText: error.response.statusText,
      errors: error.response.status === UNPROCESSABLE_ENTITY ? error.response.data.errors : null
    })
  })
}