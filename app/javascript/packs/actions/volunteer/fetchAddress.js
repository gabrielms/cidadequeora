import axios from 'axios'

export default function fetchAddress(localization, callback) {
  return (dispatch, getState) => {
    const emptyResponse = {
      street: "",
      neighborhood: "",
      city: "",
      state: "",
      complement: ""
    }
    if (localization === null) {
      callback(emptyResponse)
      return null
    }

    const url = `https://viacep.com.br/ws/${localization}/json`

    return axios({
      url,
      method: 'GET',
    })
    .then((response) => {
      callback({
        street: response.data.logradouro,
        neighborhood: response.data.bairro,
        city: response.data.localidade,
        state: response.data.uf,
        complement: response.data.complemento
      })
    }).catch((error) => {
      callback(emptyResponse)
    })
  }
}