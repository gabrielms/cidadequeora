export const SET_CURRENT_STEP = 'SET_CURRENT_STEP'

export function setCurrentStep(step) {
  return {
    type: SET_CURRENT_STEP,
    step
  }
}