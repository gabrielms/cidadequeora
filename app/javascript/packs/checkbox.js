import React, { PropTypes } from 'react'

const Checkbox = props => {
  return (
    <div className="form-check">
      <div className="form-check-label">
        <input
          type="checkbox"
          id={props.input.name}
          name={props.name}
          value={props.input.value}
          className="form-check-input"
          onChange={props.input.onChange}
        />
        {props.label}
      </div>
    </div>
  )
}

export default Checkbox