# == Schema Information
#
# Table name: churches
#
#  id         :integer          not null, primary key
#  address_id :integer
#  name       :string
#  leader     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Church < ApplicationRecord
  belongs_to :address

  accepts_nested_attributes_for :address
end
