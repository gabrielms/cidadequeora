# == Schema Information
#
# Table name: excluded_addresses
#
#  id                             :integer          not null, primary key
#  volunteer_id                   :integer
#  house_numbers                  :string
#  numbers_correspond_to_building :boolean
#  building_name                  :string
#  deliver_all                    :boolean
#  apartment_numbers              :string
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  zip_code                       :string
#  street                         :string
#  numbers                        :integer
#

class ExcludedAddress < ApplicationRecord
  belongs_to :volunteer
end
