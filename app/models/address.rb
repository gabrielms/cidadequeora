# == Schema Information
#
# Table name: addresses
#
#  id           :integer          not null, primary key
#  city         :string
#  complement   :string
#  house_number :integer
#  state        :string
#  street       :string
#  zip_code     :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Address < ApplicationRecord
  has_one :volunteer
end
