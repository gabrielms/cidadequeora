# == Schema Information
#
# Table name: volunteers
#
#  id                          :integer          not null, primary key
#  name                        :string
#  email                       :string
#  cell_phone                  :string
#  cell_phone_area_code        :string
#  residential_phone           :string
#  residential_phone_area_code :string
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  address_id                  :integer
#  church_id                   :integer
#

class Volunteer < ApplicationRecord
  belongs_to :address
  belongs_to :church

  has_many :excludes, class_name: "ExcludedAddress"

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :church
end
