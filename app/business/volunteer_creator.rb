class VolunteerCreator
  def initialize(params)
    @params = params
  end

  def create!
    volunteer_address = Address.new(params[:address_attributes])
    volunteer_address.save

    church_address = Address.new(params[:address_attributes][:church_attributes])
    church_address.save

    volunteer_church = Church.new(params[:church_attributes])
    volunteer_church.save!

    volunteer = Volunteer.new(
      name: params[:name],
      email: params[:email],
      address: volunteer_address,
      church: volunteer_church
    )

    if params[:excluded_addresses_attributes].any?
      params[:excluded_addresses_attributes].each do |excluded_address|
        volunteer.excludes.build(excluded_address)
      end
    end
    volunteer.save!

    volunteer
  end

  private

  attr_accessor :params

end