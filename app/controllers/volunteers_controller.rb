class VolunteersController < ApplicationController
  def new; end

  def create
    volunteer_creator = VolunteerCreator.new(permited_params)

    if volunteer_creator.create!
      render status: :created, json: volunteer_creator.to_json
    else
      render status: :unprocessable_entity, json: { errors: volunteer_creator.errors.full_messages }
    end
  end

  def permited_params
    params.require(:volunteer)
          .permit(:name, :email,
                  address_attributes: [
                    :city,
                    :complement,
                    :house_number,
                    :state,
                    :street,
                    :zip_code
                  ],
                  church_attributes: [
                    :name,
                    :leader,
                    address_attributes: [
                      :city,
                      :complement,
                      :house_number,
                      :state,
                      :street,
                      :zip_code
                    ]
                  ],
                  excluded_addresses_attributes: [
                    :apartment_numbers,
                    :building_name,
                    :deliver_for_all,
                    :numbers,
                    :numbers_correspond_to_building,
                    :street,
                    :zip_code
                  ])
  end
end
