class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # Just for testing purposes
  skip_before_action :verify_authenticity_token
end
