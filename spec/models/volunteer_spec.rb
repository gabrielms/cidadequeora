# == Schema Information
#
# Table name: volunteers
#
#  id                          :integer          not null, primary key
#  name                        :string
#  email                       :string
#  cell_phone                  :string
#  cell_phone_area_code        :string
#  residential_phone           :string
#  residential_phone_area_code :string
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  address_id                  :integer
#  church_id                   :integer
#

require 'rails_helper'

RSpec.describe Volunteer, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
