# == Schema Information
#
# Table name: excluded_addresses
#
#  id                             :integer          not null, primary key
#  volunteer_id                   :integer
#  house_numbers                  :string
#  numbers_correspond_to_building :boolean
#  building_name                  :string
#  deliver_all                    :boolean
#  apartment_numbers              :string
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#  zip_code                       :string
#  street                         :string
#  numbers                        :integer
#

require 'rails_helper'

RSpec.describe ExcludedAddress, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
