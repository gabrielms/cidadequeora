require 'rails_helper'

describe "creating volunteer", :type => :feature do
  it "allows me to be signed in as an volunteer" do
    visit '/'
    fill_in 'Nome', with: 'Gabriel Matos de Souza'
    fill_in 'E-mail', with: 'gabrielmatosdesouza@gmail.com'
    fill_in 'cell_phone_area_code', with: '48'
    fill_in 'cell_phone', with: '99888278'
    fill_in 'residential_phone_area_code', with: '48'
    fill_in 'residential_phone', with: '998882320'
    fill_in 'zip_code', with: '88804160'
    click_button 'Próximo'

    check 'except_nearby_residences'
    fill_in 'numbers', with: '10, 20, 30, 40'

    click_button 'Próximo'

    fill_in 'Nome da igreja', with: 'Igreja teste'
    fill_in 'CEP', with: '95560000'
    fill_in 'Logradouro', with: 'Rua teste'
    fill_in 'Nome do líder principal', with: 'Líder da igreja'

    click_button 'Salvar'

    expect(page).to have_content('Seu cadastro foi realizado com sucesso!')

  end
end