/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/packs-test/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 373);
/******/ })
/************************************************************************/
/******/ ({

/***/ 306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CURRENT_STEP", function() { return SET_CURRENT_STEP; });
/* harmony export (immutable) */ __webpack_exports__["setCurrentStep"] = setCurrentStep;
var SET_CURRENT_STEP = 'SET_CURRENT_STEP';

function setCurrentStep(step) {
  return {
    type: SET_CURRENT_STEP,
    step: step
  };
}

/***/ }),

/***/ 349:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADDRESS_REGISTER_STEP", function() { return ADDRESS_REGISTER_STEP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NEARBY_RESIDENCES_STEP", function() { return NEARBY_RESIDENCES_STEP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHURCH_STEP", function() { return CHURCH_STEP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "COMPLETED_STEP", function() { return COMPLETED_STEP; });
var ADDRESS_REGISTER_STEP = 1;
var NEARBY_RESIDENCES_STEP = 2;
var CHURCH_STEP = 3;
var COMPLETED_STEP = 4;

/***/ }),

/***/ 373:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["default"] = currentStep;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__constants_steps_js__ = __webpack_require__(349);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__actions_volunteer_currentStep_js__ = __webpack_require__(306);



var initialState = {
  step: __WEBPACK_IMPORTED_MODULE_0__constants_steps_js__["ADDRESS_REGISTER_STEP"]
};

function currentStep() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments[1];

  switch (action.type) {
    case __WEBPACK_IMPORTED_MODULE_1__actions_volunteer_currentStep_js__["SET_CURRENT_STEP"]:
      return { step: action.step };
    default:
      return state;
  }
}

/***/ })

/******/ });