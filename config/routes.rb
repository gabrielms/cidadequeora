Rails.application.routes.draw do
  root 'volunteers#new'

  resources :volunteers, only: [:new, :create]
end
