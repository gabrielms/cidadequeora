class CreateExcludedAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :excluded_addresses do |t|
      t.references :volunteer, foreign_key: true
      t.string :house_numbers
      t.boolean :numbers_correspond_to_building
      t.string :building_name
      t.boolean :deliver_all
      t.string :apartment_numbers

      t.timestamps
    end
  end
end
