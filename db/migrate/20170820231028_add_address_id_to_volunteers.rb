class AddAddressIdToVolunteers < ActiveRecord::Migration[5.1]
  def change
    add_reference :volunteers, :address, foreign_key: true
  end
end
