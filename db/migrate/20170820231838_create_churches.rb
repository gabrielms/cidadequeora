class CreateChurches < ActiveRecord::Migration[5.1]
  def change
    create_table :churches do |t|
      t.references :address, foreign_key: true
      t.string :name
      t.string :leader

      t.timestamps
    end
  end
end
