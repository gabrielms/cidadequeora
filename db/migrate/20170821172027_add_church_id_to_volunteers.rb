class AddChurchIdToVolunteers < ActiveRecord::Migration[5.1]
  def change
    add_reference :volunteers, :church, foreign_key: true
  end
end
