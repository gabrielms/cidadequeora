class AddMissingColumnsToExcludedAddresses < ActiveRecord::Migration[5.1]
  def change
    add_column :excluded_addresses, :zip_code, :string
    add_column :excluded_addresses, :street, :string
    add_column :excluded_addresses, :numbers, :integer
  end
end
