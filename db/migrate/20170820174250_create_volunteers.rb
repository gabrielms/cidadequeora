class CreateVolunteers < ActiveRecord::Migration[5.1]
  def change
    create_table :volunteers do |t|
      t.string :name
      t.string :email
      t.string :cell_phone
      t.string :cell_phone_area_code
      t.string :residential_phone
      t.string :residential_phone_area_code

      t.timestamps
    end
  end
end
