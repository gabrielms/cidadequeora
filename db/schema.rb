# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170822024258) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "city"
    t.string "complement"
    t.integer "house_number"
    t.string "state"
    t.string "street"
    t.string "zip_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "churches", force: :cascade do |t|
    t.bigint "address_id"
    t.string "name"
    t.string "leader"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["address_id"], name: "index_churches_on_address_id"
  end

  create_table "excluded_addresses", force: :cascade do |t|
    t.bigint "volunteer_id"
    t.string "house_numbers"
    t.boolean "numbers_correspond_to_building"
    t.string "building_name"
    t.boolean "deliver_all"
    t.string "apartment_numbers"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "zip_code"
    t.string "street"
    t.integer "numbers"
    t.index ["volunteer_id"], name: "index_excluded_addresses_on_volunteer_id"
  end

  create_table "volunteers", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "cell_phone"
    t.string "cell_phone_area_code"
    t.string "residential_phone"
    t.string "residential_phone_area_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "address_id"
    t.bigint "church_id"
    t.index ["address_id"], name: "index_volunteers_on_address_id"
    t.index ["church_id"], name: "index_volunteers_on_church_id"
  end

  add_foreign_key "churches", "addresses"
  add_foreign_key "excluded_addresses", "volunteers"
  add_foreign_key "volunteers", "addresses"
  add_foreign_key "volunteers", "churches"
end
